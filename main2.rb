
require_relative 'rulebook/engine'
require 'yaml'

#srand 1

@engine = Engine.new []

@engine.load ['common', 'named', 'creature', 'hit_points', 'class', 'level', 'armor_class',
             'proficiency', 'weapon', 'equipment', 'attack', 'damage', 'fighter', 'initiative',
             'encounter', 'turn']

def make_fighter name, scores, weapon
  fighter = @engine.new_creature name: name, scores: scores 

  fighter_class = @engine.fighter_class

  fighter.add_level class: fighter_class
  fighter.add_level class: fighter_class
  fighter.add_level class: fighter_class
  fighter.add_level class: fighter_class
  fighter.add_level class: fighter_class
  fighter.regain_hp amount: 100

  fighter.add_proficiency with: weapon
  fighter.equip weapon: weapon
  
  fighter
end

weapons = Hash[YAML.load_file('database/weapons.yaml').map do |name,data|
  [name.to_sym, @engine.new_weapon(data)]
end]

f1 = make_fighter "bob", { str: 18, con: 14, dex: 12 }, weapons[:longsword]
f2 = make_fighter "dude", { str: 16, con: 10, dex: 18 }, weapons[:bluc]

n1 = 0
n2 = 0

(1..200).each do |i|
  encounter = @engine.run_encounter creatures: [f1, f2]
  n1 += 1 if f2.hp == 0
  n2 += 1 if f1.hp == 0
  f1.regain_hp amount: 99
  f2.regain_hp amount: 99
end

p "bob  #{n1}"
p "dude #{n2}"

