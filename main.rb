
require_relative 'rulebook/engine'

engine = Engine.new []

engine.load ['common', 'named', 'creature', 'hit_points', 'class', 'level', 'armor_class',
             'proficiency', 'weapon', 'equipment', 'attack', 'damage', 'fighter', 'initiative',
             'encounter', 'turn']

fighter = engine.new_creature name: "bob", scores: { str: 15, con: 14 }

p fighter
p fighter.name
p fighter.dex
p fighter.cha_mod
p fighter.classes.map { |c| c.name }

fighter_class = engine.fighter_class
p fighter_class.name

fighter.add_level class: fighter_class

p fighter.classes.map { |c| c.name }

fighter.add_level class: fighter_class

p fighter.classes.map { |c| c.name }

p fighter.proficiency_bonus

fighter.add_level class: fighter_class
fighter.add_level class: fighter_class
fighter.add_level class: fighter_class

p fighter.proficiency_bonus

longsword = engine.new_weapon name: "longsword", dice_num: 1, dice_size: 8

fighter.add_proficiency with: longsword

p fighter.proficient with: longsword
p fighter.proficient with: :maul

p longsword.damage_description

p ((1..100).map do
  fighter.weapon_attack_roll weapon: longsword
end.sum() / 100.0)

p ((1..100).map do
  fighter.weapon_damage_roll weapon: longsword
end.sum() / 100.0)

defender = engine.new_creature name: "Defender", scores: { dex: 20 }

p ((1..100).map do
  fighter.attack_check weapon: longsword, target: defender
end.count { |x| x == true })

p fighter.hp_description
fighter.regain_hp amount: 100
p fighter.hp_description
p fighter.dead?

fighter.equip weapon: longsword
fighter.attack target: defender

p defender.hp_description

