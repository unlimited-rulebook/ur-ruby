
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :turn do
    def when
      record.is? @entity, :creature and record.is? @encounter, :encounter
    end

    def apply previous
      #actions = @entity.available_actions encounter: @encounter
      target = @encounter.all_creatures.find { |e| e != @entity }
      @entity.attack target: target
    end
  end

end

