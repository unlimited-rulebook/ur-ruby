
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  fighter_class = record.create_entity
  record.set_many(fighter_class, :named, name: "Fighter")
  record.set_many(fighter_class, :class, hit_dice_size: 10)

  record.define_singleton_rule :fighter_class, fighter_class

end

