
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :ac do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      10 + @entity.dex_mod
    end
  end

  record.define_rule :ac do
    def when
      record.is? @entity, :weapon
    end

    def apply previous
      10
    end
  end

end


