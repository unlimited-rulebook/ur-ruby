
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :class, hit_dice_size: 8

  record.define_rule :hit_dice_size do
    def when
      record.is? @entity, :class
    end

    def apply previous
      record.get @entity, :class, :hit_dice_size
    end
  end

end

