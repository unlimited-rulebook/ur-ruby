
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :proficient, with: []

  record.define_rule :new_creature do
    def when
      true
    end

    def apply previous
      e = previous.call
      record.set(e, :proficient, :with, [])
      e
    end
  end

  record.define_rule :proficiency_bonus do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      2
    end
  end

  record.define_rule :proficiency_bonus do
    def when
      record.is? @entity, :creature and record.is? @entity, :leveled
    end

    def apply previous
      2 + (@entity.level - 1) / 4
    end
  end

  record.define_rule :proficient do
    def when
      record.is? @entity, :creature and record.is? @entity, :proficient
    end

    def apply previous
      (record.get @entity, :proficient, :with).include? @with
    end
  end

  record.define_rule :add_proficiency do
    def when
      record.is? @entity, :creature and record.is? @entity, :proficient
    end

    def apply previous
      (record.get @entity, :proficient, :with) << @with
    end
  end

  record.define_rule :proficiency_bonus do
    def when
      record.is? @entity, :creature and record.is? @entity, :proficient and not @with.nil?
    end

    def apply previous
      0
    end
  end

  record.define_rule :proficiency_bonus do
    def when
      record.is? @entity, :creature and record.is? @entity, :proficient and not @with.nil? \
                                    and @entity.proficient with: @with
    end

    def apply previous
      @entity.proficiency_bonus
    end
  end

end

