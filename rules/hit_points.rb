
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :hp, amount: 1

  record.define_rule :new_creature do
    def when
      true
    end

    def apply previous
      e = previous.call
      record.set(e, :hp, :amount, e.max_hp)
      e
    end
  end

  record.define_rule :max_hp do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      8 + @entity.con_mod
    end
  end

  record.define_rule :max_hp do
    def when
      record.is? @entity, :creature and record.is? @entity, :leveled
    end

    def apply previous
      classes = @entity.classes
      first = classes.shift
      maxhp = first.hit_dice_size
      maxhp += classes.map { |c| c.hit_dice_size / 2 + 1 }.sum
      maxhp + @entity.level * @entity.con_mod
    end
  end

  record.define_rule :hp do
    def when
      record.is? @entity, :creature and record.is? @entity, :hp
    end

    def apply previous
      record.get @entity, :hp, :amount
    end
  end

  record.define_rule :regain_hp do
    def when
      record.is? @entity, :creature and record.is? @entity, :hp and not @amount.nil?
    end

    def apply previous
      new_amount = [@entity.max_hp, (record.get @entity, :hp, :amount) + @amount].min
      record.set @entity, :hp, :amount, new_amount
    end
  end

  record.define_rule :hp_description do
    def when
      record.is? @entity, :creature and record.is? @entity, :hp
    end

    def apply previous
      "#{@entity.hp}/#{@entity.max_hp}"
    end
  end

  record.define_rule :dead? do
    def when
      record.is? @entity, :creature and record.is? @entity, :hp
    end

    def apply previous
      @entity.hp <= 0
    end
  end

  record.define_rule :apply_damage do
    def when
      record.is? @entity, :creature and record.is? @entity, :hp and not @amount.nil?
    end

    def apply previous
      new_amount = [0, (record.get @entity, :hp, :amount) - @amount].max
      record.set @entity, :hp, :amount, new_amount
    end
  end

end

