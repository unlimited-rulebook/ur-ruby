
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :equipped, weapon: nil

  record.define_rule :equip do
    def when
      record.is? @entity, :creature and record.is? @weapon, :weapon
    end

    def apply previous
      record.set_many @entity, :equipped, weapon: @weapon
    end
  end

  record.define_rule :equipped_weapon do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      nil
    end
  end

  record.define_rule :equipped_weapon do
    def when
      record.is? @entity, :creature and record.is? @entity, :equipped
    end

    def apply previous
      record.get @entity, :equipped, :weapon
    end
  end

end

