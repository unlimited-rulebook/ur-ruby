
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :weapon_damage_roll do
    def when
      record.is? @entity, :creature and record.is? @weapon, :weapon and not @check.nil?
    end

    def apply previous
      damage_dice = @weapon.damage_dice
      num = damage_dice[:dice_num]
      num *= 2 if @check == :critical
      (1..num).map { rand(1..damage_dice[:dice_size]) }.sum() + @entity.str_mod
    end
  end

end


