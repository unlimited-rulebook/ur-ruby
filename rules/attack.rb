
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :attack_dice_roll do
    def when
      true
    end

    def apply previous
      roll = rand(1..20)
      return :fumble if roll == 1
      return :critical if roll == 20
      return roll
    end
  end

  record.define_rule :attack_bonus do
    def when
      record.is? @entity, :creature and not @with.nil?
    end

    def apply previous
      #TODO add @associated_abscore
      @entity.proficiency_bonus(with: @with) + @entity.str_mod
    end
  end

  record.define_rule :attack_check do
    def when
      record.is? @entity, :creature and not @with.nil? and record.is? @target, :creature
    end

    def apply previous
      roll = @entity.attack_dice_roll
      return roll if roll == :critical
      return :miss if roll == :fumble
      if roll + (@entity.attack_bonus with: @with) >= @target.ac
        return :hit
      else
        return :miss
      end
    end
  end

  record.define_rule :attack do
    def when
      record.is? @entity, :creature and record.is? @target, :creature
    end

    def apply previous
      check = @entity.attack_check target: @target, with: :unarmed
      @target.apply_damage amount: (damage + @entity.str_mod)
    end
  end

  record.define_rule :attack do
    def when
      record.is? @entity, :creature and record.is? @entity, :equipped \
                                    and record.is? @target, :creature
    end

    def apply previous
      weapon = @entity.equipped_weapon
      check = @entity.attack_check target: @target, with: weapon
      if check != :miss
        damage = @entity.weapon_damage_roll weapon: weapon, check: check
        @target.apply_damage amount: damage
      end
    end
  end

end

