
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :roll_initiative do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      rand(1..20) + @entity.dex_mod
    end
  end

end

