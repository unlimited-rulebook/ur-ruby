
require_relative '../rulebook/ruleset'

include RuleSet

new_ruleset do |record|

  record.new_property :named, name: "unnamed"

  record.define_rule :name do
    def when
      record.is? @entity, :named
    end

    def apply previous
      record.get @entity, :named, :name
    end
  end

end

