
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :creature, str: 10, dex: 10, con: 10, int: 10, wis: 10, cha: 10

  def make_getter_rule record, field_name

    record.define_rule field_name do

      capture field_name: field_name

      def when
        record.is? @entity, :creature
      end

      def apply previous
        record.get(@entity, :creature, field_name)
      end

    end

    record.define_rule((field_name.to_s + "_mod").to_sym) do

      capture field_name: field_name

      def when
        record.is? @entity, :creature
      end

      def apply previous
        (@entity.send(field_name) - 10) / 2
      end

    end
  end

  [:str, :dex, :con, :int, :wis, :cha].each do |field_name|
    make_getter_rule record, field_name
  end

  record.define_rule :new_creature do
    def when
      true
    end

    def apply previous
      e = record.create_entity
      record.set(e, :named, :name, @name)
      record.set_many(e, :creature, **@scores)
      e
    end
  end

end

