
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :weapon, dice_num: 1, dice_size: 4

  record.define_rule :new_weapon do
    def when
      true
    end

    def apply previous
      e = record.create_entity
      record.set(e, :named, :name, @name)
      record.set_many(e, :weapon, dice_num: @dice_num, dice_size: @dice_size)
      e
    end
  end

  record.define_rule :damage_dice do
    def when
      record.is? @entity, :weapon
    end

    def apply previous
      {
        dice_num: record.get(@entity, :weapon, :dice_num),
        dice_size: record.get(@entity, :weapon, :dice_size),
      }
    end
  end

  record.define_rule :damage_description do
    def when
      record.is? @entity, :weapon
    end

    def apply previous
      damage_dice = @entity.damage_dice
      "#{damage_dice[:dice_num]}d#{damage_dice[:dice_size]}"
    end
  end
end

