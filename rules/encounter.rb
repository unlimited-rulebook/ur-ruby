
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :encounter, current: 0, order: []

  record.define_rule :run_encounter do
    def when
      @creatures.all? { |e| record.is? e, :creature }
    end

    def apply previous
      encounter = record.create_entity
      order = @creatures.sort_by { |e| -e.roll_initiative }
      record.set_many encounter, :encounter, order: order
      
      until encounter.encounter_ended do
        break if encounter.play_round == :ended
      end
    end
  end

  record.define_rule :play_round do
    def when
      record.is? @entity, :encounter
    end

    def apply previous
      (record.get @entity, :encounter, :order).each do |e|
        e.turn encounter: @entity
        return :ended if @entity.encounter_ended
      end
    end
  end

  record.define_rule :all_creatures do
    def when
      record.is? @entity, :encounter
    end

    def apply previous
      (record.get @entity, :encounter, :order).clone
    end
  end

  record.define_rule :encounter_ended do
    def when
      record.is? @entity, :encounter
    end

    def apply previous
      creatures = @entity.all_creatures
      creatures.count { |e| e.dead? } >= creatures.size - 1
    end
  end

  record.define_rule :current? do
    def when
      record.is? @entity, :encounter and record.is? @creature, :creature
    end

    def apply previous
      order = record.get @entity, :encounter, :order
      current = record.get @entity, :encounter, :current
      order[current] == @creature
    end
  end

end

