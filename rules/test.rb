
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.define_rule :dummy do
    def when
      true
    end

    def apply previous
      e = record.create_entity
      record.set(e, :named, :name, @name)
      record.set_many(e, :creature, str: 15, dex: 14, con: 13, int: 12, wis: 10, cha: 8)
      e
    end
  end

end

