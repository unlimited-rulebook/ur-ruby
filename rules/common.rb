
require_relative '../rulebook/ruleset'

include RuleSet

new_ruleset do |record|

  record.define_rule :name do
    def when
      true
    end

    def apply previous
      @entity.to_s
    end
  end

  record.define_rule :precedes do
    def when
      record.is? @rule1, :rule and record.is? @rule2, :rule
    end

    def apply previous
      @rule1.id <=> @rule2.id
    end
  end

end

