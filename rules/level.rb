
require_relative '../rulebook/ruleset'
require_relative '../rulebook/entity'

include RuleSet

new_ruleset do |record|

  record.new_property :leveled, classes: []

  record.define_rule :level do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      0
    end
  end

  record.define_rule :level do
    def when
      record.is? @entity, :creature and record.is? @entity, :leveled
    end

    def apply previous
      record.get(@entity, :leveled, :classes).size
    end
  end

  record.define_rule :classes do
    def when
      record.is? @entity, :creature
    end

    def apply previous
      []
    end
  end

  record.define_rule :classes do
    def when
      record.is? @entity, :creature and record.is? @entity, :leveled
    end

    def apply previous
      record.get(@entity, :leveled, :classes).clone
    end
  end

  record.define_rule :add_level do
    def when
      record.is? @entity, :creature and record.is? @class, :class
    end

    def apply previous
      record.set @entity, :leveled, :classes, [@class]
    end
  end

  record.define_rule :add_level do
    def when
      record.is? @entity, :creature and record.is? @entity, :leveled and record.is? @class, :class
    end

    def apply previous
      (record.get @entity, :leveled, :classes) << @class
    end
  end

end

