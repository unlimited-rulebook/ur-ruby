
require_relative 'idgenerator.rb'

class Entity

  attr_reader :id

  def initialize record
    @id = IDGenerator::generate
    @record = record
  end

  def to_s
    "Entity: #{@id}"
  end

  def inspect
    to_s
  end

  def method_missing m, *args, &block
    params = args.size > 0 ? args.first : {}
    params[:entity] = self
    Solver::apply @record, m, **params
  end

end

