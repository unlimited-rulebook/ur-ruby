
module Solver

  def self.bootstrap record
    record.new_property :rule, keyword: "undefined", definition: false
  end

  def self.apply record, rulename, **args
    possible_cases = record.where(:rule, keyword: rulename)
    if possible_cases.size == 0 then
      return p "no such rule: #{rulename}"
    end
    matched_cases = []
    possible_cases.each do |e|
      definition = record.get(e, :rule, :definition)
      applied_rule = definition.new(**args)
      raise unless applied_rule.respond_to? :when and applied_rule.respond_to? :apply
      matched_cases.push [applied_rule, e] if applied_rule.when
    end
    if matched_cases.size > 0 then
      if matched_cases.size == 1 then
        return matched_cases.first.first.apply(->{})
      else
        return solve_conflict record, matched_cases
      end
    else
      p "no rule case applies: #{rulename}"
    end
  end

  def self.solve_conflict record, matched_cases
    matched_cases.sort! do |x1, x2|
      self.apply record, :precedes, **{ rule1: x1[1], rule2: x2[1] }
    end
    apply = nil
    matched_cases.each do |applied, rule|
      previous = apply.nil? ? ->{} : apply
      apply = -> { applied.apply previous }
    end
    apply.call
  end

end

