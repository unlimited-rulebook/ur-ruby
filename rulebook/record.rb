
require_relative 'entity'

class Record

  def initialize
    @properties = {}
    @specs = {}
    Rule.class_variable_set :@@record, self
    @rule_class = Class.new(Rule)
  end

  def new_property name, **spec
    @specs[name] = spec
    @properties[name] = {}
  end

  def set entity, name, field, value
    property = get_property! entity, name
    property[field] = value
  end

  def set_many entity, name, **fields
    property = get_property! entity, name
    @specs[name].each_key do |key|
      if fields.has_key? key
        property[key] = fields[key]
      end
    end
  end

  def is? entity, name
    not @properties[name][entity].nil?
  end
  
  def get entity, name, field
    if is? entity, name
      @properties[name][entity][field]
    end
  end

  def all name
    @properties[name].keys
  end

  def where name, **match
    all(name).filter do |entity|
      match.all? do |key, value|
        get(entity, name, key) == value
      end
    end
  end

  def define_rule name, &block
    rule = create_entity
    definition = Class.new(@rule_class, &block)
    definition.define_method :initialize do |**args|
      args.each do |k, v|
        instance_variable_set ("@"+k.to_s).to_sym, v
      end
    end
    set_many rule, :rule, keyword: name, definition: definition
  end

  def define_singleton_rule name, singleton
    define_rule name do

      capture singleton: singleton

      def initialize **args
      end

      def when
        true
      end

      def apply previous
        singleton
      end

    end
  end

  def create_entity
    Entity.new self
  end

  private

  class Rule
    def record
      @@record
    end
    def self.capture **args
      args.each do |k,v|
        self.define_method k do
          v
        end
      end
    end
  end

  def get_property! entity, name
    @properties[name][entity].nil? ? create!(entity, name) : @properties[name][entity]
  end

  def create! entity, name
    property = @specs[name].clone
    @properties[name][entity] = property
  end

end

