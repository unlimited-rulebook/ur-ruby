
require_relative 'record'
require_relative 'solver'
require_relative 'ruleset'

class Engine

  def initialize ruleset_folders
    @record = Record.new
    @folders = ruleset_folders
    @folders.push('../rules')
    Solver::bootstrap(@record)
  end

  def load rulesets
    rulesets.each do |ruleset|
      @folders.each do |folder|
        begin
          require_relative folder + "/" + ruleset
          break
        rescue
        end
      end
    end
    RuleSet::load_all(@record)
  end

  def apply keyword, **args
    Solver::apply @record, keyword, **args
  end

  def method_missing m, *args, &block
    if args.size > 0
      apply m, **args.first
    else
      apply m, **{}
    end
  end

end

