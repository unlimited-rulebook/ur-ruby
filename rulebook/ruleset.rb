
module RuleSet

  @@rulesets = []

  def new_ruleset &block
    @@rulesets.push(block)
  end

  def load_all(record)
    @@rulesets.each do |set|
      set.call record
    end
  end

end

